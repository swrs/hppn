require "test_helper"

class Hppn::DatesTest < ActiveSupport::TestCase

  setup do
    @event = Event.create
  end

  test "responds to :dates method" do
    assert @event.respond_to?(:dates) 
  end

  test "responds to :dates_start method" do
    assert @event.respond_to?(:dates_start) 
  end

  test "responds to :dates_end method" do
    assert @event.respond_to?(:dates_end) 
  end

  test "set dates as a range of dates" do
    dates = Date.today..Date.today + 1.day
    @event.update dates: dates

    assert_equal dates.begin...dates.end + 1.day, @event.reload.dates,
      "equals to dates range with exclusive bounds"
  end

  test "set dates as string of %Y-%m-%d dates" do
    dates = Date.today..Date.today + 1.day
    @event.update dates: "#{dates.begin.strftime('%Y-%m-%d')} - #{dates.end.strftime('%Y-%m-%d')}"

    assert_equal dates.begin...dates.end + 1.day, @event.reload.dates,
      "equals to dates range with exclusive bounds"
  end

  test "set dates with dates_start and dates_end" do
    dates_start = Date.today
    dates_end = dates_start + 1
    @event.update dates_start: dates_start, dates_end: dates_end
    assert_equal dates_start...dates_end + 1.day, @event.reload.dates
  end

  test "get start when dates == nil" do
    assert_nil @event.dates_start
  end

  test "get start when start == Date and end == nil" do
    dates_start = Date.today
    @event.update dates: dates_start..nil
    assert_equal dates_start, @event.reload.dates_start
  end

  test "get start when start == Date and end == Date" do
    dates_start = Date.today
    dates_end = dates_start + 1.day
    @event.update dates: dates_start..dates_end
    assert_equal dates_start, @event.reload.dates_start
  end

  test "get start when start == nil and end == Date" do
    @event.update dates: ..Date.today
    assert_nil @event.reload.dates_start
  end

  test "set start as Date" do
    dates_start = Date.today
    @event.update dates_start: dates_start
    assert_equal dates_start...Float::INFINITY, @event.reload.dates
    assert_equal dates_start, @event.reload.dates_start
  end

  test "set start as String" do
    dates_start = Date.today
    @event.update dates_start: dates_start.strftime("%Y-%m-%d")
    assert_equal dates_start, @event.reload.dates_start
  end

  test "set start as empty String" do
    @event.update dates_start: ""
    assert_nil @event.reload.dates_start
  end

  test "get end when dates == nil" do
    assert_nil @event.dates_end
  end

  test "get end when start == nil and end == Date" do
    dates_end = Date.today
    @event.update dates: nil..dates_end
    assert_equal dates_end, @event.reload.dates_end
  end

  test "get end when start == Date and end == Date" do
    dates_start = Date.today
    dates_end = dates_start + 1.day
    @event.update dates: dates_start..dates_end
    assert_equal dates_end, @event.reload.dates_end
  end

  test "get end when start == Date and end == nil" do
    @event.update dates: (Date.today..)
    assert_nil @event.reload.dates_end
  end

  test "set end as Date" do
    dates_end = Date.today
    @event.update dates_end: dates_end
    assert_equal -Float::INFINITY...dates_end + 1.day, @event.reload.dates
    assert_equal dates_end, @event.dates_end
  end

  test "set end as String" do
    dates_end = Date.today
    @event.update dates_end: dates_end.strftime("%Y-%m-%d")
    assert_equal dates_end, @event.reload.dates_end
  end

  test "set end as empty String" do
    @event.update dates_end: ""
    assert_nil @event.reload.dates_end
  end

  test "queries finite date periods" do
    Event.destroy_all
    yesterday = Event.create(dates: Date.today - 2.days..Date.today - 1.day) 
    today = Event.create(dates: Date.today - 1.day..Date.today + 1.day) 
    tomorrow = Event.create(dates: Date.today + 1.day..Date.today + 1.days) 

    assert_equal [yesterday, today, tomorrow], Event.dates_asc.to_a,
      "events by dates in asc order"

    assert_equal [tomorrow, today, yesterday], Event.dates_desc.to_a,
      "events by dates in desc order"
    
    assert_equal 1, Event.dates_past.size, "has one past event"
    assert_includes Event.dates_past, yesterday, "pas events includes yesterday"
    assert_equal 1, Event.dates_today.size, "has one event today"
    assert_includes Event.dates_today, today, "today events includes today"
    assert_equal 1, Event.dates_upcoming.size, "has one upcoming event"
    assert_includes Event.dates_upcoming, tomorrow, "upcoming events includes tomorrow"
  end
end
