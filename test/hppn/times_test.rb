require "test_helper"

class Hppn::TimesTest < ActiveSupport::TestCase

  setup do
    @event = Event.create
  end

  test "responds to :times method" do
    assert @event.respond_to?(:times) 
  end

  test "responds to :times_start method" do
    assert @event.respond_to?(:times_start) 
  end

  test "responds to :times_end method" do
    assert @event.respond_to?(:times_end) 
  end

  test "responds to :times_shift method" do
    assert @event.respond_to?(:times_shift) 
  end

  test "Translator raises exception for wrong kind" do
    assert_raises ArgumentError do
      Hppn::Times::Translator.new([]).to_time
    end
  end

  test "Translator raises exception for wrong string format" do
    assert_raises ArgumentError do
      Hppn::Times::Translator.new("foo").to_time
    end
  end

  test "Translator parses correct timezone" do
    zone = Time.zone
    testzone = "CET"
    Time.zone = testzone
    time = Time.zone.now
    string = time.strftime("%H:%M")
    t_number = Hppn::Times::Translator.new(string).to_number
    t_time = Hppn::Times::Translator.new(t_number).to_time
    assert_equal time.hour, t_time.hour
    assert_equal testzone, t_time.zone
    Time.zone = zone
  end

  test "set times as range of time" do
    times_start = Time.parse("12:00")
    times_end = times_start + 1.hour
    @event.update times: times_start..times_end

    assert_equal times_start.seconds_since_midnight.to_i...(times_end.seconds_since_midnight.to_i + 1),
      @event.reload.times, "equals to exclusive range of 'seconds since midnight'"
  end

  test "set times as range of time (across midnight)" do
    times_start = Time.parse("23:00")
    times_end = Time.parse("01:00")
    @event.update times: times_start..times_end

    assert_equal times_start.seconds_since_midnight.to_i...(times_end.seconds_since_midnight.to_i + 24*60*60 + 1),
      @event.reload.times, 
      "equals to exclusive range of 'seconds since midnight' with gap of one day"
  end

  test "set times as range of seconds since midnight" do
    times_start = Time.parse("12:00")
    times_end = times_start + 1.hour
    @event.update times: times_start.seconds_since_midnight..times_end.seconds_since_midnight

    assert_equal times_start.seconds_since_midnight.to_i...(times_end.seconds_since_midnight.to_i + 1),
      @event.reload.times, "equals to exclusive range of 'seconds since midnight'"
  end

  test "set times as range without end" do
    times_start = Time.parse("12:00")
    @event.update times: times_start..nil
    assert_equal times_start.seconds_since_midnight.to_i...Float::INFINITY, 
      @event.reload.times,
      "equals to exclusive range of 'seconds since midnight' with infinite end"
  end

  test "set times as range without start" do
    times_end = Time.parse("12:00")
    @event.update times: nil..times_end

    assert_equal -Float::INFINITY...times_end.seconds_since_midnight.to_i + 1, 
      @event.reload.times,
      "equals to exclusive range of 'seconds since midnight' with infinite start"
  end

  test "set times with times_start and _end" do
    times_start = Time.parse("12:00")
    times_end = times_start + 1.hour
    @event.update times_start: times_start, times_end: times_end

    assert_equal times_start.seconds_since_midnight.to_i...times_end.seconds_since_midnight.to_i + 1,
      @event.reload.times, 
      "equals to exclusive range of 'seconds since midnight'"
  end

  test "get times_start when times = nil" do
    assert_nil @event.times_start
  end

  test "get times_start when start is infinite" do
    @event.update times_end: Time.zone.now
    assert_nil @event.reload.times_start
  end

  test "get/set times_start when times_end = nil" do
    times_start = Time.zone.now
    @event.update times: times_start..nil

    assert_equal Tod::TimeOfDay(times_start), @event.reload.times_start,
      "returns Tod::TimeOfDay for times_start"
  end

  test "get/set times_start when times_end is set" do
    times_start = Time.parse("12:00")
    times_end = Time.parse("13:00")
    @event.update times_start: times_start, times_end: times_end
    assert_equal Tod::TimeOfDay(times_start), @event.reload.times_start
  end

  test "get/set times_start as string" do
    @event.update times_start: "12:00"
    assert_equal Tod::TimeOfDay(Time.parse("12:00")), @event.reload.times_start
  end

  test "get/set times_start as Tod::TimeOfDay" do
    tod = Tod::TimeOfDay(Time.zone.now)
    @event.update times_start: Tod::TimeOfDay(tod) 
    assert_equal tod, @event.reload.times_start
  end

  test "set times_start to nil" do
    @event.update times_start: Time.zone.now
    @event.update times_start: nil
    assert_nil @event.times_start
  end

  test "get times_end when times = nil" do
    assert_nil @event.times_end
  end

  test "get/set times_end when times_end = nil" do
    times_end = Time.zone.now
    @event.update times: nil..times_end

    assert_equal Tod::TimeOfDay(times_end), @event.reload.times_end,
      "returns Tod::TimeOfDay for times_end"
  end

  test "get/set times_end when times_start is set" do
    times_start = Time.parse("12:00")
    times_end = Time.parse("13:00")
    @event.update times_start: times_end, times_end: times_end
    assert_equal Tod::TimeOfDay(times_end), @event.reload.times_end
  end

  test "set times_end as string" do
    @event.update times_end: "12:00"
    assert_equal Tod::TimeOfDay(Time.parse("12:00")), @event.reload.times_end
  end

  test "get/set times_end as Tod::TimeOfDay" do
    tod = Tod::TimeOfDay(Time.zone.now)
    @event.update times_end: Tod::TimeOfDay(tod) 
    assert_equal tod, @event.reload.times_end
  end

  test "set times_end to nil" do
    @event.update times_end: Time.zone.now
    @event.update times_end: nil
    assert_nil @event.times_end
  end

  test "get times_shift with only times_start set" do
    times_start = Time.parse("12:00")
    @event.times_start = times_start
    assert_nil @event.times_shift
  end

  test "get times_shift with times_start and _end set" do
    times_start = Time.parse("12:00")
    @event.update times_start: times_start, times_end: times_start + 1.hour
    assert @event.reload.times_shift.is_a?(Tod::Shift)
    assert_equal 60*60, @event.reload.times_shift.duration
  end

  test "orders by times" do
    Event.destroy_all
    breakfast = Event.create(times: Time.parse("7:00")..Time.parse("8:00"))
    lunch = Event.create(times: Time.parse("12:00")..Time.parse("13:00"))
    dinner = Event.create(times: Time.parse("20:00")..Time.parse("22:00"))

    assert_equal [breakfast, lunch, dinner], Event.times_asc.to_a,
      "events by times in asc order"

    assert_equal [dinner, lunch, breakfast], Event.times_desc.to_a,
      "events by times in decs order"
  end

  test "queries past, current, and upcoming" do
    @event.update times: Time.zone.now - 1.hour..Time.zone.now - 30.minutes
    assert_includes Event.times_past, @event, "past events includes @event"
    assert_not_includes Event.times_now, @event, "current events do not include @event"
    assert_not_includes Event.times_upcoming, @event, 
      "upcoming events do not include @event"
    @event.update times: Time.zone.now - 30.minutes..Time.zone.now + 30.minutes
    assert_not_includes Event.times_past, @event, "past events do not include @event"
    assert_includes Event.times_now, @event, "current events includes @event"
    assert_not_includes Event.times_upcoming, @event, 
      "upcoming events do not include @event"
    @event.update times: Time.zone.now + 1.hour..Time.zone.now + 1.hour
    assert_not_includes Event.times_past, @event, "past events do not include @event"
    assert_not_includes Event.times_now, @event, "current events do not include @event"
    assert_includes Event.times_upcoming, @event, "upcoming events includes @event"
  end

  test "queries endless time periods" do
    @event.update times_start: Time.zone.now
    assert_includes Event.times_now, @event
    assert_not_includes Event.times_upcoming, @event
    assert_not_includes Event.times_past, @event
  end

  test "queries startless time periods" do
    @event.update times_end: Time.zone.now
    assert_includes Event.times_now, @event
    assert_not_includes Event.times_upcoming, @event
    assert_not_includes Event.times_past, @event
  end
end
