class CreateEvents < ActiveRecord::Migration[6.1]
  def change
    create_table :events do |t|
      t.daterange :dates
      t.int4range :times

      t.timestamps
    end
  end
end
