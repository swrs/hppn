class Event < ApplicationRecord
  include Hppn::Dates
  dates attr: :dates

  include Hppn::Times
  times attr: :times
end
