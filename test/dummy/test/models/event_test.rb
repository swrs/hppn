require "test_helper"

class EventTest < ActiveSupport::TestCase

  test "query event by date and time" do
    event = Event.create(dates: Date.today..Date.today)
    assert_includes Event.dates_today, event, "dates_today includes event"
    assert_not_includes Event.dates_today.times_now, event, "times_now does not include event"
    event.update times_start: Time.zone.now
    assert_includes Event.dates_today.times_now, event, "times_now includes event"
    event.update times_end: event.times_start - 60*60, dates_end: Date.today + 1.day

    assert_includes Event.dates_today.times_now, event, 
      "times_now includes event spanning across midnight"

    event.update dates_start: Date.today - 1.day
    assert_includes Event.dates_today, event
    event.update dates_end: event.dates_start
    assert_not_includes Event.dates_today, event
    assert_includes Event.dates_past.times_now, event
  end
end
