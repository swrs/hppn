# Changelog

## 1.0.3
- use `Time.zone.now.to_date` in scopes for `Hppn::Dates`

## 1.0.0
- Initial release of `Hppn` with `Hppn::Times` and `Hppn::Dates`
