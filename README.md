# Hppn

Hppn is a [Ruby on Rails](https://github.com/rails/rails) plugin to facilitate the handling of models' dates and times. In other words, a very simple set of functions to store and retrieve date and time information from and to a database column.

## Requirements

- [Ruby on Rails](https://github.com/rails/rails) (>= 6.0.0) application using PostgreSQL
- [Ruby](https://www.ruby-lang.org) (>= 3.0.0)

## Installation

```ruby
gem "hppn", git: "https://gitlab.com/swrs/hppn", branch: "master"
```

## Usage

Hppn comes with two modules which are basically [`ActiveSupport::Concern`](https://api.rubyonrails.org/classes/ActiveSupport/Concern.html) modules: `Hppn::Dates` to handle date periods, and `Hppn::Times` to handle time periods. Those modules extend can any `ApplicationRecord` with methods to set, get, and query date and time information.

Both modules make use of Postgres' [Range Types](https://www.postgresql.org/docs/current/rangetypes.html) in order to store and query information about date and time periods. This reduces the amount of necessary columns and facilitates querying of records.

### `Hppn::Dates`

`Hppn::Dates` is used to set/get information about the start and/or end date of a model. Date information is stored as a range of dates and requires a PostgreSQL column type of `daterange`. 

```ruby
class AddTimesToEvents < ActiveRecord::Migration[6.1]
  def change
    add_column :events, :dates, :daterange
  end
end
```

Include the `Hppn::Dates` module into your model and set the attribute (column) which should be used for setting/getting start and end date information with the `dates(attr: :dates)` method.

```ruby
class Event < ApplicationRecord
  include Hppn::Dates
  dates attr: :dates # set the attribute for date informations
end
```

Following getter/setter methods, based on the name of the given `attr` (in this case `:dates`), will be available to a model instance:

```ruby
event = Event.create(dates: Date.today..Date.today + 1.day)
#=> #<Event id: 13, dates: Tue, 20 Apr 2021..Wed, 21 Apr 2021, times: nil, created_at: "2021-04-20 03:55:32.593182000 -0500", updated_at: "2021-04-20 03:55:32.593182000 -0500">
# dates can be set as a range of Date objects
event.reload.dates
#=> Tue, 20 Apr 2021...Thu, 22 Apr 2021
# note how PostgreSQL stores ranges with exclusive bounds, Hppn::Dates will take care
# of converting exclsive to inclusive bounds
event.update(dates: "2021-04-15 - 2021-04-16")
#=> Thu, 15 Apr 2021...Sat, 17 Apr 2021
# dates can be set as a String in format "%Y-%m-%d - %Y-%m-%d"

event.dates_start
#=> Thu, 15 Apr 2021
# dates_start getter returns the beginning of :dates range
event.dates_end
#=> Fri, 16 Apr 2021 
# dates_end getter returns the end of :dates range, note how the exclusive
# bound of :dates (Sat, 17 Apr 2021) was converted to an inclusive bound, the
# ending day (Fri, 16 Apr 2021)

event.update(dates_start: Date.parse("2021-06-06"), dates_end: "2021-06-12")
event.reload.dates
#=> Sun, 06 Jun 2021...Sun, 13 Jun 2021
event.dates_start
#=> Sun, 06 Jun 2021
event.dates_end
#=> Sat, 12 Jun 2021

event.update(dates_end: nil)
event.reload.dates
#=> Sun, 06 Jun 2021...Infinity
event.dates_end
#=> nil
event.update(dates_start: nil)
event.reload.dates
#=> nil
event.dates_start
#=> nil
```

Following scopes, based on the name of the given `attr` (in this case `:dates`), will be available to the model:

```ruby
Event.dates_asc      
# query records with dates in ascending order
Event.dates_desc     
# query records with dates in descending order
Event.dates_past     
# query records where current date > dates_end
Event.dates_today      
# query records where current date is within dates range
Event.dates_upcoming 
# query records where current date < dates_start
```

### `Hppn::Times`

`Hppn::Times` is used to set/get information about the start and/or end time of a model. Time information is stored as a range of "seconds since midnight" and requires a PostgreSQL column type of `int4range`. This module makes heavy use of the great [`Tod`](https://github.com/jackc/tod).

```ruby
class AddTimesToEvents < ActiveRecord::Migration[6.1]
  def change
    add_column :events, :times, :int4range
  end
end
```

Include the `Hppn::Times` module into your model and set the attribute (column) which should be used for setting/getting start and end time information with the `times(attr: :times)` method.

```ruby
class Event < ApplicationRecord
  include Hppn::Times
  times attr: :times # set the attribute for time informations
end
```

Following getter/setter methods, based on the name of the given `attr` (in this case `:times`), will be available to a model instance:

```ruby
event = Event.create(times: Time.parse("12:00")..Time.parse("13:00"))
#=> #<Event id: 9, dates: nil, times: 43200..46800, created_at: "2021-04-20 03:04:29.419847000 -0500", updated_at: "2021-04-20 03:04:29.419847000 -0500">
# times are always converted to "seconds since midnight"
event.times == (12*60*60..13*60*60)
#=> true
event.reload.times
#=> 43200...46801
# PostgreSQL stores ranges with exclusive bounds, Hppn::Times takes care of converting
# exclusive to inclusive bounds when getting time start and end information
event.update(times: Time.parse("12:00")..)
event.times
#=> 43200..
event.reload.times
#=> 43200...Infinity
# PostgreSQL stores infinite ranges with Infinity

event.update(times_start: Time.parse("8:00"), times_end: "9:00")
# set times using the times_start and times_end setters
# which can receive Time, String ("%Y-%m-%d"), Integer (seconds since midnight), 
# and Tod::TimeOfDay objects
event.reload.times
#=> 28800...32401

event.times_start
#=> #<Tod::TimeOfDay:0x00005651706460f0 @hour=8, @minute=0, @second=0, @second_of_day=28800>
event.times_end
#=> #<Tod::TimeOfDay:0x00005651707f5c48 @hour=9, @minute=0, @second=0, @second_of_day=32400>
# times_start and times_end getters return Tod::TimeOfDay objects
event.times_start.to_s
#=> "08:00:00"
event.times_end.to_s
#=> "09:00:00"

event.update(times_end: nil)
event.reload.times
#=> 82800...Infinity
event.reload.times_end
#=> nil
# Infinity bounds return nil for times_start and times_end
event.update(times_start: nil)
event.reload.times
#=> nil

event.update(times_start: "23:00", times_end: "01:00")
event.reload.times
#=> 82800...90001
event.times == (23*60*60..24*60*60+1*60*60)
#=> true
# times ranges work across midnight, when times_end < times_start, it assumes
# that times_end is on the next day

event.times_shift
#=> #<Tod::Shift:0x000055c72477ed68 @beginning=#<Tod::TimeOfDay:0x000055c72477ef98 @hour=23, @minute=0, @second=0, @second_of_day=82800>, @ending=#<Tod::TimeOfDay:0x000055c72477ed90 @hour=1, @minute=0, @second=0, @second_of_day=3600>, @exclude_end=false, @range=82800..90000>
# returns a Tod::Shift object which is helpful to handle and compare time durations

dinner = Event.create(times: Time.parse("20:00")..Time.parse("23:00"))
desert = Event.create(times: Time.parse("22:00")..Time.parse("22:30"))
dinner.times_shift.overlaps?(desert.times_shift)
#=> true
dinner.times_shift.include?(desert.times_start)
#=> true
```

Following scopes, based on the name of the given `attr` (in this case `:times`), will be available to the model:

```ruby
Event.times_asc      
# query records with times in ascending order
Event.times_desc     
# query records with times in descending order
Event.times_past     
# query records where current time > times_end
Event.times_now      
# query records where current time is within times range
Event.times_upcoming 
# query records where current time < times_start
```

## Testing

The plugin comes with tests under `test` and a dummy application under `test/dummy` for test and development purposes. The dummy application will need a PostgreSQL database and two databases `dummy_development` and `dummy_test`. In order to keep the databases ephemeral you can make use of the docker compose `postgres` service provided under `.docker`; the dummy application is already configured to talk to the postgres database on port `5433` (instead of the default `5432`). Postgres' data will be saved at `.docker/postgres/data`. Initially you will have to create the necessary databases

```bash
$ cd hppn
$ docker-compose -f ./.docker/docker-compose.yml up postgres
$ cd test/dummy && bin/rails db:create && cd ../../ # if databases do not yet exist
$ bin/test # to run test suite
$ cd test/dummy && bin/rails # to execute rails tasks
```

## Credits

- The `Hppn::Times` module makes heavy use of the [Tod](https://github.com/jackc/tod) gem.
