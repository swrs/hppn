module Hppn
  module Dates
    extend ActiveSupport::Concern

    included do
      class_attribute :dates_attr
    end

    class_methods do
      def dates(attr: :dates)
        attr = attr.to_sym
        self.dates_attr = attr

        scope "#{attr}_asc".to_sym, -> { order(attr => :asc) }
        scope "#{attr}_desc".to_sym, -> { order(attr => :desc) }

        scope "#{attr}_past".to_sym, -> { where("upper(#{attr}) <= ?::date", 
          Time.zone.now.to_date) } 

        scope "#{attr}_today".to_sym, -> { where("#{attr} @> ?::date", 
          Time.zone.now.to_date) } 

        scope "#{attr}_upcoming".to_sym, -> { where("lower(#{attr}) > ?::date", 
          Time.zone.now.to_date) } 

        # <attr>=(value)
        # receives a Range of Date objects or a String as value
        # a String is converted to a Range of Dates using parse_dates(string)
        define_method("#{attr}=") do |value|
          return value if value == self[attr] 

          if value.is_a?(String)
            value = Hppn::Dates::DatesTranslator.new.parse_dates(value)
          end

          if value.is_a?(Range)
            value = nil if (value.begin.nil? || value.begin.infinite?) && 
              (value.end.nil? || value.end.infinite?)
          end

          self[attr] = value
        end

        # <attr>_start=(value)
        # receives Date or String as value and updates start value 
        # for <attr> date range, a String is converted to Date 
        # using parse_date(string)
        define_method("#{attr}_start=") do |value|
          if value.is_a?(String)
            value = Hppn::Dates::DatesTranslator.new.parse_date(value)
          end

          send("#{attr}=", send(attr).nil? ? value..nil : value..send("#{attr}_end"))
        end

        # <attr>_start
        # getter for the start of <attr> date range
        # if begin == nil or infinite returns nil
        # else returns Date object
        define_method("#{attr}_start") do
          range = send(attr)
          range && range.begin && !range.begin.infinite? ? range.begin : nil
        end

        # <attr>_end=(value)
        # receives Date or String as value and updates end value 
        # for <attr> date range, a String is converted to Date 
        # using parse_date(string)
        define_method("#{attr}_end=") do |value|
          if value.is_a?(String)
            value = Hppn::Dates::DatesTranslator.new.parse_date(value)
          end

          send("#{attr}=", send(attr).nil? ? nil..value : send("#{attr}_start")..value)
        end

        # <attr>_end
        # getter for the end of <attr> date range
        # if begin == nil or infinite returns nil
        # else returns Date object
        define_method("#{attr}_end") do
          range = send(attr)

          if range && range.end && !range.end.infinite?
            range.exclude_end? ? range.end - 1.day : range.end
          else
            nil
          end
        end
      end
    end

    class DatesTranslator

      # parse dates range from string in format
      # "%Y-%m-%d - %Y-%m%d"
      def parse_dates(string)
        return nil if string.blank?
        regexp = /^\d{4}(-\d{2})?(-\d{2})?( - \d{4}(-\d{2})?(-\d{2})?)?$/   

        if regexp.match(string)
          array = string.split(" - ").map { |d| parse_date(d.strip) }
          array.first..array.last
        else
          nil
        end
      end

      def parse_date(string)
        return nil if string.blank?

        if /^\d{4}$/.match(string)
          format = "%Y"
        elsif /^\d{4}-\d{2}$/.match(string)
          format = "%Y-%m"
        else
          format = "%Y-%m-%d"
        end

        Date.strptime(string, format)
      end
    end
  end
end
