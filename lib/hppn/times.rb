require "hppn/times/translator"

module Hppn
  module Times
    extend ActiveSupport::Concern

    included do
      class_attribute :times_attr
    end

    class_methods do
      
      # set column / attribute for times (default: :times)
      def times(attr: :times)
        attr = attr.to_sym
        self.times_attr = attr

        scope "#{attr}_asc".to_sym, -> { order(attr => :asc) }
        scope "#{attr}_desc".to_sym, -> { order(attr => :desc) }

        scope "#{attr}_past".to_sym, 
          -> { where("upper(#{attr}) < ?", Time.zone.now.seconds_since_midnight.to_i) } 

        scope "#{attr}_now".to_sym, 
          -> { where("#{attr} @> ?", Time.zone.now.seconds_since_midnight.to_i) }

        scope "#{attr}_upcoming".to_sym, 
          -> { where("lower(#{attr}) > ?", Time.zone.now.seconds_since_midnight.to_i) } 

        # <attr>=(value)
        # receives a Range of Time or Numeric as value 
        # and converts it into a Range of "seconds since midnight"
        define_method("#{attr}=") do |value|
          return value if value == self[attr]

          # value is a range and has begin and end
          if value.is_a?(Range) && value.begin && value.end
            values = [value.begin, value.end]
            values.map! { |v| Hppn::Times::Translator.new(v).to_time }

            value = Tod::Shift.new(Tod::TimeOfDay(values[0]), Tod::TimeOfDay(values[1]))
              .range
          elsif value.is_a?(Range) && value.begin
            value = Hppn::Times::Translator.new(value.begin).to_number..nil
          elsif value.is_a?(Range) && value.end
            value = nil..Hppn::Times::Translator.new(value.end).to_number
          else
            value = nil
          end

          self[attr] = value
        end

        # <attr>_start=(value)
        # set start of times range as Time or String
        define_method("#{attr}_start=") do |value|
          if value.presence
            value = Hppn::Times::Translator.new(value).to_number 
          else
            value = nil
          end

          send("#{attr}=", send(attr).nil? ? value..nil : value..send("#{attr}_end", true))
        end

        # <attr>_start(integer = false)
        # getter for start value of times range
        # if end is nil or infinite => nil
        # else => end value of inclusive or exclusive range
        # returns Tod::TimeOfDay object by default or Integer (if integer == true)
        define_method("#{attr}_start") do |raw = false|
          range = send(attr)

          if range && range.begin&.finite?
            value = send(attr).begin
            return value if raw
            Tod::TimeOfDay(Hppn::Times::Translator.new(value).to_time)
          else
            nil
          end
        end

        # <attr>_end=(value)
        # set start of times range as Time or String
        define_method("#{attr}_end=") do |value|
          if value.presence
            value = Hppn::Times::Translator.new(value).to_number 
          else
            value = nil
          end

          send("#{attr}=", send(attr).nil? ? nil..value : send("#{attr}_start", true)..value)
        end

        # <attr>_end(integer = false)
        # getter for end value of times range
        # if end is nil or infinite => nil
        # else => end value of inclusive or exclusive range
        # returns Tod::TimeOfDay object by default or Integer (if integer == true)
        define_method("#{attr}_end") do |integer = false|
          range = send(attr)

          if range && range.end&.finite?
            value = range.end
            value -= 1 if range.exclude_end?
            integer ? value : Tod::TimeOfDay(Hppn::Times::Translator.new(value).to_time)
          else
            nil
          end
        end

        define_method("#{attr}_shift") do
          if send(attr) && send("#{attr}_start") && send("#{attr}_end")
            Tod::Shift.new(send("#{attr}_start"), send("#{attr}_end"))
          else
            nil
          end
        end
      end
    end
  end
end
