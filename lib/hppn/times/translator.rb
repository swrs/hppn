module Hppn
  module Times
    class Translator
      attr_reader :value

      def initialize(value)
        @value = value

        if !allowed_kinds.any? { |k| value.kind_of?(k) }
          raise ArgumentError.new("value must be of kind #{allowed_kinds.join(', ')}")
        end
      end
    
      def to_time
        return value if value.is_a?(Time)

        if value.is_a?(String)
          time = Time.zone.parse(value)
          raise ArgumentError.new, "Time.zone can not parse #{value}" if time.nil?
          return time
        end

        seconds = value.is_a?(Tod::TimeOfDay) ? value.second_of_day : value

        if [Integer, Float].any? { |k| seconds.kind_of?(k) }
          time = Time.zone.at(seconds)
          time - time.utc_offset
        end
      end
    
      def to_number
        if value.is_a?(Tod::TimeOfDay)
          value.second_of_day
        elsif value.is_a?(String)
          Time.zone.parse(value).seconds_since_midnight.to_i
        elsif value.is_a?(Time)
          value.seconds_since_midnight.to_i
        elsif [Integer, Float].any? { |k| value.kind_of?(k) }
          value.to_i
        end
      end
    
      private

      def allowed_kinds
        [Time, Integer, Float, String, Tod::TimeOfDay]
      end
    end
  end
end
