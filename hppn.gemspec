require_relative "lib/hppn/version"

Gem::Specification.new do |spec|
  spec.name        = "hppn"
  spec.version     = Hppn::VERSION
  spec.authors     = ["Swrs"]
  spec.email       = ["mail@swrs.net"]
  spec.homepage    = "https://gitlab.com/swrs/hppn"
  spec.summary     = "Hppn is a Ruby on Rails plugin to facilitate the handling of event dates and times."
  spec.description = spec.summary
  spec.license     = "MIT"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage
  spec.metadata["changelog_uri"] = "#{spec.homepage}/changelog.md"

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 7.0"
  spec.add_dependency "tod", "~> 3.0.0"
  spec.add_development_dependency "byebug", "~> 11.1.3"
end
